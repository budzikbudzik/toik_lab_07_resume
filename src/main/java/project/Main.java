package project;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;

import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

import java.io.FileNotFoundException;


public class Main {
    public static final String dest = "D:/resume.pdf";
    public static final float[] pointColumnWidths = {150F, 350F};

    public static void main(String[] args) throws FileNotFoundException {
        createPDF();

    }

    private static void createPDF() throws FileNotFoundException {
        // Creating a PdfDocument object
        PdfWriter writer = new PdfWriter(dest);

        // Creating a PdfDocument object
        PdfDocument pdf = new PdfDocument(writer);

        // Creating a Document object
        Document doc = new Document(pdf);

        //adding title
        doc.add(addTitle("Resume"));

        // Adding Table to document
        doc.add(addTable(pointColumnWidths));


        // Closing the document
        doc.close();
        System.out.println("Table created successfully..");
    }

    private static Paragraph addTitle(String text) {
        Paragraph myTitle = new Paragraph(text).setFontSize(40f).setTextAlignment(TextAlignment.CENTER);
        return myTitle;
    }

    private static Table addTable(float[] pointColumnWidths) {

        // Creating a table
        Table table = new Table(pointColumnWidths);

        // Adding cells to the table
        table.addCell(new Cell().add("First name"));
        table.addCell(new Cell().add("Kamil"));

        table.addCell(new Cell().add("Profession"));
        table.addCell(new Cell().add("Student"));

        table.addCell(new Cell().add("Education"));
        table.addCell(new Cell().add("PWSZ"));

        table.addCell(new Cell().add("Summary"));
        table.addCell(new Cell().add("\"Lorem ipsum dolor sit amet," +
                " consectetur adipiscing elit, sed do eiusmod tempor incididunt ut" +
                " labore et dolore magna aliqua. Ut enim ad minim veniam," +
                " quis nostrud exercitation ullamco laboris nisi ut aliquip" +
                " ex ea commodo consequat."));
        return table;
    }
}